# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 08:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


def migrer_artiste(apps, schema):
    # On récupère les modèles
    Morceau = apps.get_model('musiques', 'Morceau')
    Artiste = apps.get_model('musiques', 'Artiste')
    # On récupère les artistes déjà enregsitrés dans la table Morceau
    # Voir la documentation :
    # - https://docs.djangoproject.com/fr/1.11/ref/models/querysets/#all
    # - https://docs.djangoproject.com/fr/1.11/ref/models/querysets/#values
    # - https://docs.djangoproject.com/fr/1.11/ref/models/querysets/#distinct

    artistes = {m.artiste for m in Morceau.objects.all()}
    for nom in artistes:
        Artiste.objects.create(nom=nom)


def annuler_migrer_artiste(apps, schema):
    Artiste = apps.get_model('musiques', 'Artiste')
    for a in Artiste.objects.all():
        a.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('musiques', '0004_auto_20171004_0907'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='morceau',
            name='date_sortie',
        ),
        migrations.AddField(
            model_name='morceau',
            name='artiste_fk',
            field=models.ForeignKey(
                null=True, on_delete=django.db.models.deletion.CASCADE, to='musiques.Artiste'),
        ),
    ]
