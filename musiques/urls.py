from django.conf.urls import url
from .views import MorceauDetailView
from .views import *

app_name = 'musiques'
# Encapsule les urls de ce module dans le namespace musique
urlpatterns = [
    url(r'(?P<pk>\d+)$', MorceauDetailView.as_view(), name='morceau-detail'),
    url(r'^listeMorceaux', listemorceaux, name='listemorceaux')
]
